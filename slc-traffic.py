#imports

import pandas as pd
import numpy as np
import plotly.express as px

import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from matplotlib import ticker
#visualizers

# install if needed
#!pip install sktime[all_extras]

# time series tools
from sktime.forecasting.arima import ARIMA
from statsmodels.tsa.stattools import acf, pacf

# scaling tool
from sklearn.preprocessing import MinMaxScaler

df = pd.read_csv('/Users/tessa/SLC-Traffic-Predictions/datasets/Utah Traffic.csv')
df = df.rename({"Unnamed: 0": "datetime"}, axis="columns")
df["datetime"] = pd.to_datetime(df["datetime"])
df = df.set_index("datetime", drop=True)
df = df.rename({orig: int(orig[1:]) for orig in df.columns}, axis="columns")
print(df.head())

print(df.head())

#Minor changes. 
# More minor changes. 